import unittest
from update_openfisca_ai.llm import handle_token_error


class TestHandleTokenError(unittest.TestCase):
    def test_ratio_exceeds_one(self):
        # In this test, the calculated ratio exceeds 1 and should be capped to 1.
        error_message = (
            "together.error.InvalidRequestError: Error code: 422 - "
            '{"message": "Input validation error: `inputs` tokens + `max_new_tokens` must be <= 8193. Given: 2206 `inputs` tokens and 32768 `max_new_tokens`", '
            '"type_": "invalid_request_error"}'
        )
        result = handle_token_error(error_message)
        self.assertEqual(result, 1)

    def test_ratio_less_than_one(self):
        # Here, with tokens such that resulting ratio is less than 1.
        # given_tokens = 6000, max_tokens = 8000, so:
        # ratio = (8000 / (6000 + 2048)) * 0.8
        error_message = (
            "Error: must be <= 8000. Given: 6000 `inputs` tokens and other details"
        )
        result = handle_token_error(error_message)
        expected_ratio = 0.8 * (8000 / (6000 + 2048))
        self.assertAlmostEqual(result, expected_ratio, places=4)

    # ERROR: ratio exceeds 1: ratio=1.5407616361071934, Error code: 422 - {"message": "Input validation error: `inputs` tokens + `max_new_tokens` must be <= 8193. Given: 2206 `inputs` tokens and 32768 `max_new_tokens`", "type_": "invalid_request_error"}
    def test_ratio_exceeds_one_2(self):
        error_message = 'Error code: 422 - {"message": "Input validation error: `inputs` tokens + `max_new_tokens` must be <= 8193. Given: 2206 `inputs` tokens and 32768 `max_new_tokens", "type_": "invalid_request_error"}'
        result = handle_token_error(error_message)
        self.assertEqual(result, 1)


if __name__ == "__main__":
    unittest.main()
