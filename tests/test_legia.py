from update_openfisca_ai.legia import LegIA

brackets_answer = """
Si l'on considère que le taux pour la fraction inférieure ou égale à 11 294 € est effectivement de 0 %, le barème complet serait :

```json
{
    "brackets": [
        {"threshold": 0, "rate": 0},
        {"threshold": 11294, "rate": 0.11},
        {"threshold": 28797, "rate": 0.3},
        {"threshold": 82341, "rate": 0.41},
        {"threshold": 177106, "rate": 0.45}
    ]
}
```

Cependant, la formulation de la question et le contexte donné suggèrent de se concentrer sur les tranches de revenu supérieures à 11 294 €, ce qui correspond à la première réponse fournie. 

La réponse finale est donc :

```json
{
    "brackets": [
        {"threshold": 11294, "rate": 0.11},
        {"threshold": 28797, "rate": 0.3},
        {"threshold": 82341, "rate": 0.41},
        {"threshold": 177106, "rate": 0.45}
    ]
}
```
"""

brackets_answer_accident = """
Pour déterminer le nouveau barème pour 'Barème de réduction du salaire net utilisé pour le calcul des rentes post accident du travail', nous devons analyser les informations fournies dans le texte réglementaire.

Le texte indique que le salaire annuel supérieur au salaire minimum est pris en compte de la manière suivante pour le calcul de la rente :
- S'il ne dépasse pas le double du salaire minimum, il est intégralement pris en compte.
- S'il dépasse le double du salaire minimum, l'excédent n'est compté que pour un tiers.
- Il n'est pas tenu compte de la fraction excédant huit fois le montant du salaire minimum.

Cela peut être traduit en un barème avec les taux et les seuils suivants :
- Un taux de 1 (ou 100%) pour les salaires allant jusqu'au double du salaire minimum.
- Un taux de 0.3333334 (ou 1/3) pour les salaires dépassant le double du salaire minimum mais ne dépassant pas huit fois le salaire minimum.
- Un taux de 0 pour les salaires dépassant huit fois le salaire minimum.

Le barème peut donc être représenté comme suit :

```json
{
    "brackets": [
        {'rate': 1, 'threshold': 2, 'date': null},
        {'rate': 0.3333334, 'threshold': 8, 'date': null},
        {'rate': 0, 'threshold': null, 'date': null}
    ]
}
```

Cependant, pour une représentation plus standardisée et claire, il est préférable de spécifier les seuils en termes de multiples du salaire minimum, comme indiqué dans le texte. Puisque le texte ne fournit pas de date spécifique pour ces taux, nous laissons la date à `null`. 

La réponse finale est :

```json
{
    "brackets": [
        {'rate': 1, 'threshold': 2, 'date': null},
        {'rate': 0.3333334, 'threshold': 8, 'date': null},
        {'rate': 0, 'threshold': null, 'date': null}
    ]
}
```"""

def test_find_value_in_answer_brackets():
    answer = brackets_answer
    expected_value = {
        "brackets": [
            {"threshold": 11294, "rate": 0.11},
            {"threshold": 28797, "rate": 0.3},
            {"threshold": 82341, "rate": 0.41},
            {"threshold": 177106, "rate": 0.45}
        ]
    }
    result = LegIA.find_value_in_answer(answer, brackets=True, debug=True)
    assert result == expected_value


def test_final_answer_brackets():
    system_prompt = "system_prompt Quelle est la valeur attendue ?"
    user_prompt = "user_prompt La nouvelle valeur est :"
    answer = brackets_answer
    final_answer = {
        "brackets": [
            {"threshold": 11294, "rate": 0.11},
            {"threshold": 28797, "rate": 0.3},
            {"threshold": 82341, "rate": 0.41},
            {"threshold": 177106, "rate": 0.45}
        ]
    }
    brackets = True
    debug = True
    expected_result = {
                    "question": system_prompt + user_prompt,
                    "answer": answer,
                    "value": {
                        "brackets": [
                            {"threshold": 11294, "rate": 0.11},
                            {"threshold": 28797, "rate": 0.3},
                            {"threshold": 82341, "rate": 0.41},
                            {"threshold": 177106, "rate": 0.45}
                        ]
                    },
                    "error": None,
                }
    result = LegIA.final_answer(system_prompt=system_prompt, user_prompt=user_prompt, answer=answer, final_answer=final_answer, brackets=brackets, debug=debug)
    assert result == expected_result

def test_find_value_in_answer_json():
    answer = '{"valeur": 0.25}'
    expected_value = 0.25
    result = LegIA.find_value_in_answer(answer, debug=True)
    assert result == expected_value


# </think>\n\n```json\n{\n    "valeur": 42500.0\n}\n```
def test_find_value_in_answer_json_with_think():
    answer = '</think>\n\n```json\n{\n    "valeur": 42500.0\n}\n```'
    expected_value = 42500.0
    result = LegIA.find_value_in_answer(answer, debug=True)
    assert result == expected_value


def test_find_value_in_answer_dict():
    answer = {"valeur": 0.25}
    expected_value = 0.25
    result = LegIA.find_value_in_answer(answer, debug=True)
    assert result == expected_value


def test_find_value_in_answer_invalid_json():
    answer = "invalid json"
    result = LegIA.find_value_in_answer(answer, debug=True)
    assert result is None


def test_find_value_in_answer_no_value():
    answer = '{"other_key": 0.25}'
    result = LegIA.find_value_in_answer(answer, debug=True)
    assert result is None


def test_find_value_in_answer_bad_json():
    answer = 'La réponse est donc :\n\n{\n    "valeur": 0.25\n}'
    expected_value = 0.25
    result = LegIA.find_value_in_answer(answer, debug=True)
    assert result == expected_value


def test_find_value_in_answer_text():
    answer = "La nouvelle valeur est : 0.25"
    expected_value = " 0.25"
    result = LegIA.find_value_in_answer(answer, debug=True)
    assert result == expected_value


def test_find_value_in_answer_text_no_value():
    answer = "some random text"
    result = LegIA.find_value_in_answer(answer, debug=True)
    assert result is None

if __name__ == "__main__":
    #test_find_value_in_answer_brackets()
    test_final_answer_brackets()
