"""
export PYTHONPATH=$PYTHONPATH:`pwd`/dataset_generation
pytest dataset_generation/tests/*
"""

import unittest
from unittest.mock import patch
from update_openfisca_ai.utilitaires import find_value_in_texte
from update_openfisca_ai.legidb import (
    LegiDB,
    find_version_id_from_date,
    extract_id_from_url,
    get_final_url,
)

legi = LegiDB()


class TestLegiDB(unittest.TestCase):

    def test_extract_id_from_url(self):
        self.assertEqual(
            extract_id_from_url(
                "https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000038496156"
            ),
            "JORFARTI000038496156",
        )

    def test_find_version_id_from_date(self):
        ##############################################
        # Test find_version_id_from_date
        raw_article_json = legi.get_raw_article_json("LEGIARTI000038869220")
        self.assertEqual(
            find_version_id_from_date(raw_article_json, "2021-01-01"),
            "LEGIARTI000038869220",
        )

    def test_get_titre(self):
        raw_article_json = legi.get_raw_article_json("LEGIARTI000038869220")
        self.assertEqual(
            legi.get_titre(raw_article_json)["long"],
            "Décret n° 2019-797 du 26 juillet 2019 relatif au régime d'assurance chômage - Section 3 : Allocation journalière - Chapitre 4 : Détermination de l'allocation journalière - Titre I : L'ALLOCATION D'AIDE AU RETOUR À L'EMPLOI - RÈGLEMENT D'ASSURANCE CHÔMAGE - Annexe A - Annexes",
        )
        self.assertEqual(
            legi.get_titre(raw_article_json)["court"],
            "Art. 14 du Décret n° 2019-797 du 26 juillet 2019 relatif au régime d'assurance chômage",
        )
        raw_article_json = legi.get_raw_article_json("LEGISCTA000043701827")
        self.assertEqual(
            legi.get_titre(raw_article_json)["long"],
            "Code du travail - Titre IV : Financement de l'apprentissage - Livre II : L'apprentissage - Sixième partie : La formation professionnelle tout au long de la vie - Partie législative",
        )
        self.assertEqual(
            legi.get_titre(raw_article_json)["court"],
            "Code du travail - Chapitre II : Contribution supplémentaire à l'apprentissage",
        )
        raw_article_json = legi.get_raw_article_json("LEGIARTI000048723703")
        self.assertEqual(
            legi.get_titre(raw_article_json)["court"],
            "Art. D5122-13 du Code du travail",
        )
        raw_article_json = legi.get_raw_article_json("JORFTEXT000026863441")
        self.assertEqual(
            legi.get_titre(raw_article_json)["court"], "JORF n°0304 du 30 décembre 2012"
        )

    def test_get_id_version_en_vigueur(self):
        ##############################################
        # Test get_id_version_en_vigueur
        raw_article_json = legi.get_raw_article_json("LEGIARTI000038869220")
        self.assertEqual(
            legi.get_id_version_en_vigueur(raw_article_json), "LEGIARTI000038869220"
        )

    def test_get_versions(self):
        ##############################################
        # Test get_versions
        raw_article_json = legi.get_raw_article_json("LEGIARTI000038869220")
        self.assertEqual(len(legi.get_versions(raw_article_json)), 1)
        raw_article_json = legi.get_raw_article_json("LEGIARTI000049641925")
        self.assertEqual(len(legi.get_versions(raw_article_json)), 63)

    def tet_get_article(self):
        ##############################################
        # Test get_article
        article = legi.get_article("LEGIARTI000038869220", debug=False)
        self.assertEqual(article["etat"], "VIGUEUR")

    def test_get_article_from_url_and_date(self):
        ##############################################
        # Test get_article_from_url_and_date
        article = legi.get_article_from_url_and_date(
            "https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000038496156",
            "2021-01-01",
            debug=False,
        )
        self.assertEqual(article["etat"], "VIGUEUR")
        article = legi.get_article_from_url_and_date(
            "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000033817781/2001-01-01",
            "2001-01-01",
            debug=False,
        )
        self.assertEqual(article["etat"], "MODIFIE")
        article = legi.get_article_from_url_and_date(
            "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000000000000/2001-01-01",
            "2021-01-01",
            debug=False,
        )
        self.assertEqual(article, False)

    def test_get_sql_from_id(self):
        ##############################################
        # Test get_sql_from_id
        self.assertEqual(
            str(legi.get_sql_from_id("LEGIARTI000038869220")),
            "SELECT id, data FROM article WHERE id='LEGIARTI000038869220';",
        )
        self.assertEqual(
            str(legi.get_sql_from_id("LEGIARTI000042837182")),
            "SELECT id, data FROM article WHERE id='LEGIARTI000042837182';",
        )
        self.assertEqual(
            str(legi.get_sql_from_id("JORFTEXT000038496156")),
            "SELECT id, data FROM textelr WHERE id='JORFTEXT000038496156';",
        )
        self.assertEqual(
            str(legi.get_sql_from_id("LEGISCTA000038496156")),
            "SELECT id, data FROM section_ta WHERE id='LEGISCTA000038496156';",
        )
        with self.assertRaises(ValueError):
            legi.get_sql_from_id("toto")
        ##############################################
        # Test is_article_en_vigueur

    def test_is_article_en_vigueur(self):
        with self.assertRaises(ValueError):
            legi.is_article_en_vigueur("toto")
        self.assertFalse(legi.is_article_en_vigueur("LEGIARTI000042837182"))
        with self.assertRaises(KeyError):
            legi.is_article_en_vigueur("JORFARTI000038496156")
        self.assertTrue(legi.is_article_en_vigueur("LEGIARTI000038869220"))

    def test_get_all_textes(self):
        ##############################################
        # Test get_all_textes
        raw_article_json = legi.get_raw_article_json(
            "LEGISCTA000043701827", debug=False
        )
        self.assertTrue(
            legi.get_all_textes(raw_article_json)["html"].startswith(
                "<br/>   Peuvent être habilités à collect"
            )
        )
        self.assertEqual(legi.get_all_textes(raw_article_json)["nature"], "CODE")
        raw_article_json = legi.get_raw_article_json(
            "JORFTEXT000038496102", debug=False
        )
        self.assertEqual(
            legi.get_all_textes(raw_article_json)["html"][:20], "<p>\n<br/>I.-Le code "
        )
        self.assertEqual(legi.get_all_textes(raw_article_json)["nature"], "LOI")
        raw_article_json = legi.get_raw_article_json(
            "JORFTEXT000000688994", debug=False
        )
        self.assertEqual(legi.get_all_textes(raw_article_json)["html"], "")
        self.assertEqual(legi.get_all_textes(raw_article_json)["nature"], "")

    def test_get_article(self):
        ##############################################
        # Test get_article
        article = legi.get_article("LEGIARTI000038869220", debug=False)
        self.assertEqual(article["etat"], "VIGUEUR")
        article = legi.get_article("LEGISCTA000043701827", debug=False)
        self.assertEqual(article["etat"], "MULTIPLE")
        self.assertTrue(
            article["content_html"].startswith(
                "<br/>   Peuvent être habilités à collect"
            )
        )

        # Decret modificatif
        article = legi.get_article("LEGIARTI000049641925", debug=False)
        self.assertEqual(article["decret_modificatif"]["id"], "JORFTEXT000049629761")
        self.assertEqual(article["decret_modificatif"]["date"], "2024-05-30")
        self.assertEqual(
            article["decret_modificatif"]["nom"],
            "Décret n°2024-496 du 30 mai 2024 - art. 1",
        )

        # https://legal.tricoteuses.fr/textes/JORFTEXT000000701672
        # https://legal.tricoteuses.fr/textelr/JORFTEXT000000701672
        article = legi.get_article_from_url_and_date(
            url="https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000701672",
            date="2021-01-01",
            debug=False,
        )
        # article = legi.get_article("JORFTEXT000000701672", debug=True)
        self.assertEqual(article["etat"], "MULTIPLE")
        self.assertEqual(
            article["content_html"][:41], "<br/>   A titre exceptionnel et provisoir"
        )
        article = legi.get_article("LEGIARTI000044981935", debug=False)
        self.assertTrue(" 0,44 % " in article["content_html"])
        self.assertEqual(
            find_value_in_texte(article["content_html"], 0.0044, debug=True), "0,44"
        )

        article = legi.get_article_from_url_and_date(
            url="https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000046816473/2024-01-01/",
            date="2025-01-01",
            debug=False,
        )
        self.assertEqual(
            find_value_in_texte(article["content_html"], 192.3, debug=False), "192,3"
        )

        # Type LEGISTA
        article = legi.get_article_from_url_and_date(
            "https://www.legifrance.gouv.fr/codes/id/LEGISCTA000043701827",
            "2015-01-01",
            debug=False,
        )
        self.assertEqual(article["etat"], "MULTIPLE")
        self.assertGreater(len(article["content_html"]), 5000)
        # Type JORFSCTA
        article = legi.get_article_from_url_and_date(
            "https://www.legifrance.gouv.fr/jorf/id/JORFSCTA000036580381",
            "2021-01-01",
            debug=False,
        )
        self.assertEqual(
            article["content_html"][:40], "<p>Article 6<br/>\nEmplois en faveur des "
        )

        # Gestion des dates de version sur LEGIARTI
        article = legi.get_article_from_url_and_date(
            "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000036679615/",
            "2018-01-01",
            debug=False,
        )
        self.assertEqual(find_value_in_texte(article["content_html"], 0.13), "13")

    @patch("update_openfisca_ai.legidb.extract_id_from_url")
    @patch.object(LegiDB, "get_raw_article_json")
    @patch.object(LegiDB, "get_id_version_en_vigueur")
    def test_find_id_articles_en_vigueur(
        self,
        mock_get_id_version_en_vigueur,
        mock_get_raw_article_json,
        mock_extract_id_from_url,
    ):
        # Setup mock return values
        mock_extract_id_from_url.side_effect = lambda url: url.split("/")[-1]
        mock_get_raw_article_json.side_effect = lambda id: {"id": id}
        mock_get_id_version_en_vigueur.side_effect = (
            lambda article_json: article_json["id"] + "_vigueur"
        )

        legidb = LegiDB()
        urls = [
            "https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000038496156",
            "https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000042683772",
        ]
        expected_ids = ["JORFARTI000038496156_vigueur", "LEGIARTI000042683772_vigueur"]

        for url, expected_id in zip(urls, expected_ids):
            self.assertEqual(legidb.find_id_articles_en_vigueur([url]), [expected_id])

    def test_find_id_articles_en_vigueur_ir(self):
        urls = [
            "https://github.com/openfisca/openfisca-france/blob/master/openfisca_france/parameters/impot_revenu/bareme_ir_depuis_1945/bareme.yaml",
            "https://www.legifrance.gouv.fr/jorf/id/JORFARTI000042753593",  # LOI n° 2020-1721 du 29 décembre 2020 de finances pour 2021
            "https://www.legifrance.gouv.fr/eli/loi/2019/12/28/CPAX1925229L/jo/texte",  # LOI n° 2019-1479 du 28 décembre 2019 de finances pour 2020
            "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000046860759/2024-01-01/",  # Code modifié par la loi de finance 2024
            "https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000048727355",  # LOI n° 2023-1322 du 29 décembre 2023 de finances pour 2024
        ]
        expected_id = ["LEGIARTI000051212954"]  # Code modifié par la loi de finance 2025
        id = legi.find_id_articles_en_vigueur(urls)
        self.assertEqual(id, expected_id)

    def test_find_id_articles_en_vigueur_travail(self):
        urls = [
            "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000026294815/2012-08-18/",
            "https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000026290023/2025-02-21",
        ]
        expected_id = ["LEGIARTI000044056633"]
        id = legi.find_id_articles_en_vigueur(urls)
        self.assertEqual(id, expected_id)

    def test_find_id_articles_en_vigueur_redirect(self):
        urls=['https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019202997&cidTexte=LEGITEXT000006069577']
        # expected_id = "LEGIARTI000037099472"
        expected_id = []  # Car "LEGIARTI000037099472" est ABROGE
        id = legi.find_id_articles_en_vigueur(urls)
        self.assertEqual(id, expected_id)

    def test_get_final_url(self):
        url='https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019202997&cidTexte=LEGITEXT000006069577'
        expected_url = "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000037099472/2025-02-24"
        stop = len("https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000037099472/")
        final = get_final_url(url)
        self.assertEqual(final[:-stop], expected_url[:-stop])
        

if __name__ == "__main__":
    unittest.main()
