# Liste des PR ouvertes sur openfisca-france dans le cadre de ce projet

Total changed files across all PRs as of February 2025: 510 !

## 2024

1. [Mise à jour last_value_still_valid_on impôt sur le revenu pour les références périmées](https://github.com/openfisca/openfisca-france/pull/2294)
1. [Revalorisations APL étudiants](https://github.com/openfisca/openfisca-france/pull/2287)
1. [Rattrapage de l'historique de prestations_sociales.aides_logement.logement_social.plu.par_categorie_de_menage](https://github.com/openfisca/openfisca-france/pull/2308)
1. [Revalorisation des aides aux logement en métropole et outre-mer](https://github.com/openfisca/openfisca-france/pull/2297)
1. [Mise à jour des last_value_still_valid_on pour la taxation indirecte](https://github.com/openfisca/openfisca-france/pull/2293)
1. [Mise à jour des last_value_still_valid_on pour la taxation capital](https://github.com/openfisca/openfisca-france/pull/2291)
1. [Mise à jour des _last_value_still_valid_on_ pour les prestations sociales](https://github.com/openfisca/openfisca-france/pull/2290)
1. [Mise à jour des _last_value_still_valid_on_ pour les prélèvements sociaux](https://github.com/openfisca/openfisca-france/pull/2289)
1. [Mise à jour des last_value_still_valid_on pour l'impôt sur le revenu](https://github.com/openfisca/openfisca-france/pull/2288)
1. [Mise à jour des urls des références Unedic](https://github.com/openfisca/openfisca-france/pull/2276)
1. [Expand and update accise_energie_metropole](https://github.com/openfisca/openfisca-france-indirect-taxation/pull/183)
1. [Mise à jour des paramètres d'imposition des rentes viagères](https://github.com/openfisca/openfisca-france/pull/2332) - A trouvé la référence automatiquement en utilisant un agent.
1. [Indemnité de résidence dans la fonction publique](https://github.com/openfisca/openfisca-france/pull/2335) - N'a pas réussi à trouvé tout seul la référence législative.
1. [Neutralise crédit impôt pour assurance loyer impayé assloy](https://github.com/openfisca/openfisca-france/pull/2314)

Total changed files across all 2025 PRs: 205

## 2025

1. [MAJ last_value_still_valid_on pour les taxation_societes](https://github.com/openfisca/openfisca-france/pull/2426)
1. [MAJ des last_value_still_valid_on sur le RSA](https://github.com/openfisca/openfisca-france/pull/2427)
1. [MAJ reductions_cotisations_sociales](https://github.com/openfisca/openfisca-france/pull/2458)
1. [MAJ contributions_sociales](https://github.com/openfisca/openfisca-france/pull/2457)
1. [MAJ last_value_still_valid_on de l'IR](https://github.com/openfisca/openfisca-france/pull/2428)
1. [MAJ prestations_etat_de_sante](https://github.com/openfisca/openfisca-france/pull/2456)
1. [MAJ last_value_still_valid_on contributions_assises_specifiquement_accessoires_salaire](https://github.com/openfisca/openfisca-france/pull/2454)
1. [MAJ des last_value_still_valid_on de chomage.allocations_assurance_chomage](https://github.com/openfisca/openfisca-france/pull/2453)
1. [MAJ des last_value_still_valid_on de l'IFI](https://github.com/openfisca/openfisca-france/pull/2452)
1. [MAJ last_value_still_valid_on du versement transport en Ile-de-France](https://github.com/openfisca/openfisca-france/pull/2430)
1. [MAJ last_value_still_valid_on prestations familialles](https://github.com/openfisca/openfisca-france/pull/2450)
1. [Mise à jour des last_value_still_valid_on pour les Aides au logement](https://github.com/openfisca/openfisca-france/pull/2446)
1. [Mise à jour des montants des Aides au logement](https://github.com/openfisca/openfisca-france/pull/2447)
1. [MAJ des last_value_still_valid_on prestations_sociales.prestations_familiales](https://github.com/openfisca/openfisca-france/pull/2449)
1. [MAJ des last_value_still_valid_on des Prestations Sociales de Solidarite et Insertion](https://github.com/openfisca/openfisca-france/pull/2451)
1. [MAJ prelevements_sociaux.professions_liberales](https://github.com/openfisca/openfisca-france/pull/2455)
1. [MAJ impot_revenu.calcul_reductions_impots](https://github.com/openfisca/openfisca-france/pull/2459)
1. [MAJ impot_revenu.calcul_revenus_imposables](https://github.com/openfisca/openfisca-france/pull/2460)

Total changed files across all 2025 PRs: 305
