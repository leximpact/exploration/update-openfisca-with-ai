"""
Script to look at the numbers of files updated with AI.
It use a markdown files with link to PR as an input and output the total number
of modified files.
"""

import re
import sys
import time
import requests
from decouple import Config, RepositoryEnv
try:
    env_config = Config(RepositoryEnv("../.env"))
except FileNotFoundError:
    env_config = Config(RepositoryEnv(".env"))

def extract_github_pr_urls(markdown_content):
    """Extract GitHub PR URLs from markdown content."""
    pattern = r'https://github\.com/[^/\s]+/[^/\s]+/pull/\d+'
    return re.findall(pattern, markdown_content)

def get_files_changed_count(url, github_token=None):
    """Get the number of changed files using GitHub API."""
    # Extract owner, repo, and PR number from URL
    match = re.match(r'https://github\.com/([^/]+)/([^/]+)/pull/(\d+)', url)
    if not match:
        print(f"Invalid GitHub PR URL: {url}")
        return 0
    
    owner, repo, pr_number = match.groups()
    api_url = f"https://api.github.com/repos/{owner}/{repo}/pulls/{pr_number}"
    
    headers = {'Accept': 'application/vnd.github.v3+json'}
    if github_token:
        headers['Authorization'] = f'token {github_token}'
    
    try:
        response = requests.get(api_url, headers=headers)
        response.raise_for_status()
        pr_data = response.json()
        return pr_data.get('changed_files', 0)
    except requests.RequestException as e:
        print(f"API Error for {url}: {str(e)}")
        return 0

def main():
    if len(sys.argv) != 2:
        print("Usage: python script.py <path_to_markdown_file>")
        sys.exit(1)
    
    markdown_file = sys.argv[1]
    github_token = env_config('GITHUB_TOKEN', None)
    
    if not github_token:
        print("Warning: No GITHUB_TOKEN found. Unauthenticated requests are limited to 60/hour.")
    
    try:
        with open(markdown_file, 'r', encoding='utf-8') as f:
            content = f.read()
    except FileNotFoundError:
        print(f"Error: File '{markdown_file}' not found.")
        sys.exit(1)
    
    pr_urls = extract_github_pr_urls(content)
    if not pr_urls:
        print("No GitHub PR URLs found.")
        return
    
    total_files = 0
    for url in pr_urls:
        print(f"Processing {url}...")
        count = get_files_changed_count(url, github_token)
        total_files += count
        if not github_token:
            print("Use a GITHUB_TOKEN to go faster.")
            time.sleep(1)
    
    print(f"\nTotal changed files across all PRs: {total_files}")

if __name__ == "__main__":
    main()