from num2words import num2words
from bs4 import BeautifulSoup
import re


def extract_dates(text: str):
    date_regex = r"\b(?:[0-9]{4}[-/\\][0-9]{2}[-/\\][0-9]{2})\b"
    return re.findall(date_regex, text)


def formatNumber(num):
    """
    Format a number to an integer if it is an integer.
    Avoids having floats with .0
    """
    if num % 1 == 0:
        return int(num)
    else:
        return num


def extract_floats(text: str, debug: bool = False) -> list[float]:
    """
    Return a list of floats found in the text.
    """
    if isinstance(text, float) or isinstance(text, int):
        return [text]
    # pattern = r"-?\d+(?:[ \.]?\d{3})*(?:[.,]\d+)?"
    pattern = r"-?\d+(?:[\,]?\d{3})*(?:[.]\d+)?"

    numbers = re.findall(pattern, text)
    cleaned_numbers = []
    for num in numbers:
        try:
            cleaned_numbers.append(float(num.replace(",", "")))
        except ValueError:
            print("ValueError", num)
            continue
    return cleaned_numbers


def surlignage(texte, value):
    """
    Permet de surligner une valeur dans un texte HTML.
    """
    if value:
        new_text = f'<mark>{str(value).replace(".",",")}</mark>'
        texte = texte.replace(str(value).replace(".", ","), new_text)
        if len(texte) > 2000:
            first = texte.find(new_text)
            last = texte.rfind(new_text)
            # texte = texte[max(0, pos - 500) : min(len(texte), pos + 500)]
            texte = (
                "[...]"
                + texte[max(0, first - 400) : min(len(texte), last + 400)]
                + "[...]"
            )
    return texte


def find_value_in_texte(
    txt_to_search: str, value_to_find: float, detail: bool = False, debug: bool = False
):
    """
    Return the value found in text.
    Try to find different format of the value in the text.
    If the value is not found, return False.
    """

    try:
        value_to_find = float(value_to_find)
    except ValueError:
        print(f"find_value_in_text(): {value_to_find} is not a number !")
        return False

    formats = [
        lambda x: str(x),
        lambda x: f"{x * 100:,}".replace(",", " "),
        lambda x: f"{x * 100:,.3f}".replace(".", ","),
        lambda x: f"{x * 100:,.2f}".replace(",", " "),
        lambda x: f"{x * 100:,.2f}".replace(",", " ").replace(".", ","),
        lambda x: f"{x * 100:,}".replace(",", " ").replace(".", ","),
        lambda x: f"{x * 100:,.0f}".replace(",", " ").replace(".", ","),
        lambda x: f"{x * 100:,.2f}".replace(",", " ").replace(".", ","),
        lambda x: f"{x * 100:,.1f}".replace(",", " ").replace(".", ","),
        lambda x: f"{x:,.2f}".replace(".", ","),
        lambda x: str(x).replace(".", ","),
        lambda x: str(x).replace(".0", ""),
        lambda x: f"{x:,}".replace(",", " "),
        lambda x: f"{x:,}".replace(",", " ").replace(".", ","),
        lambda x: f"{x:,}".replace(",", " ").replace(".0", ""),
        lambda x: "demi" if x == 0.5 else "",
        lambda x: num2words(x, lang="fr") if 0 < x < 101 else "",
    ]

    for format_func in formats:
        value = format_func(value_to_find)
        if value == "":
            continue
        if debug:
            print(f"Try with {value}")
        start = txt_to_search.find(value)
        if start >= 0:
            if debug:
                print(f"Found {value_to_find=} with {value=}")
            # Use a regexp to find the value in the text with spaces or tabs as prefix, dot or comma as separator, and space as suffix
            regexps = [
                re.compile(r"\D" + re.escape(value) + r"[\s%]\D"),
                re.compile(r"\D" + re.escape(value) + r"[<\s,\.$\-]\D"),
            ]
            for regexp in regexps:
                if debug:
                    print(f"Try with {regexp=}")
                match = regexp.search(txt_to_search)
                if match:
                    if debug:
                        print(f"Found {value_to_find=} with {regexp=}")
                    start = match.start()
                    stop = match.end()
                    if detail:
                        return {
                            "value_found": txt_to_search[start + 1 : stop - 2],
                            "in_texte": txt_to_search[
                                max(0, start - 30) : min(stop + 10, len(txt_to_search))
                            ],
                        }
                    else:
                        return txt_to_search[start + 1 : stop - 2]
            else:
                if debug:
                    print(f"Found {value_to_find=} as {value=} but not with regexp !")
                return False
    if debug:
        print(f"Value {value_to_find} not found in text.")
    return False


def get_text_from_html(html):
    """
    Retourne le texte contenu dans le html.
    """
    return BeautifulSoup(html, features="lxml").text


def clean_texte(texte: str) -> str:
    # Nettoye le texte pour ne pas casser le Markdown
    new_texte = re.sub(r"\s+", " ", get_text_from_html(texte))
    new_texte = new_texte.replace("|", " ")
    new_texte = new_texte.replace(">", " ")
    new_texte = new_texte.replace("<", " ")
    return new_texte
