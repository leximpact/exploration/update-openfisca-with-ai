from unittest import TestCase
from update_openfisca_ai.llm import get_completion
from update_openfisca_ai.utilitaires import extract_dates, extract_floats
import update_openfisca_ai.prompts as prompts
import json
import re  # Added for regex support
import traceback
from update_openfisca_ai.openfisca_param import (
    is_brackets_equal,
)

tc = TestCase()


def _iterate_search_last_value_still_valid_on(
    legia, param_of: dict, article: dict, new_legal_text: str = None
) -> dict:
    """
    new_legal_text : Allow to put the text of many article to give the model a better understanding.
    """
    article_id = article["id"]
    if new_legal_text is None:
        new_legal_text = article["content_html"]
    if param_of["is_brackets"]:
        llm_answer = legia.llm_find_brakets(
            description=f'{param_of["description"]} - {param_of["name"]} - {param_of["documentation"]}',
            new_legal_text=new_legal_text,
            current_brackets=param_of["current_bracket"],
            debug=True,
        )
    else:
        llm_answer = legia.llm_find_value(
            description=f'{param_of["description"]} - {param_of["name"]} - {param_of["documentation"]}',
            new_legal_text=new_legal_text,
            existing_value=param_of["value"],
            debug=False,
        )
    if llm_answer.get("value") == param_of["value"]:
        print(
            "La valeur est déjà à jour, il faut juste changer la date de vérification"
        )
        return {
            "status": "still_valid",
            "data": {
                "param_of": param_of,
                "article": article,
                "llm_answer": llm_answer,
            },
        }
    if param_of["is_brackets"]:
        new = llm_answer.get("value").get("brackets")
        previous = param_of["current_bracket"]
        if is_brackets_equal(previous, new):
            print("The new brackets are equal to the previous ones.")
        else:
            print(
                "La valeur du barème a changé, il faudra changer la valeur, la date et la référence."
            )
            return {
                "status": "update",
                "data": {
                    "param_of": param_of,
                    "article": article,
                    "llm_answer": llm_answer,
                },
            }
    else:
        if llm_answer.get("value") is None:
            error = f"La valeur n'a pas été trouvée dans {article_id} pour {param_of["github_url"]} !"
            print(error)
            return {
                "status": "error",
                "error": error,
                "data": {
                    "param_of": param_of,
                    "article": article,
                    "llm_answer": llm_answer,
                },
            }
        else:
            print(
                "La valeur a changé, il faudra changer la valeur, la date et la référence."
            )
            return {
                "status": "update",
                "data": {
                    "param_of": param_of,
                    "article": article,
                    "llm_answer": llm_answer,
                },
            }


def search_last_value_still_valid_on(
    legidb,
    legia,
    param_of,
    root_path="../../openfisca-france/",
    code_only: bool = False,
):
    param_path = param_of["file_path"]
    try:
        # On cherche dans tous les articles
        # Si il y a un ou plusieurs codes, on ne cherche que dans les codes.
        urls = []
        for i in range(12):
            url = param_of[f"url_ref_{i}"]
            if url == url:
                urls.append(param_of[f"url_ref_{i}"])
        article_ids = legidb.find_id_articles_en_vigueur(urls, code_only)
        if code_only and len(article_ids) == 0:
            msg = "Pas de CODE dans les références d'articles !"
            print(msg)
            return {
                "status": "error",
                "error": msg,
                "data": {"param_of": param_of},
            }
        new_legal_text = ""
        last_article = None
        for article_id in article_ids:
            article = legidb.get_article(article_id)
            if not article:
                print(f"{param_of['name']} - Article non trouvé {article_id}")
                continue
            new_legal_text += "```\n" + article["titre"] + "\n"
            new_legal_text += article["titre_court"] + "\n"
            new_legal_text += article["content_html"] + "\n```\n--------------------\n"
            last_article = article
        if last_article:
            answer = _iterate_search_last_value_still_valid_on(
                legia, param_of, last_article, new_legal_text
            )
        else:
            msg = "Pas d'article trouvé dans les références d'articles !"
            print(msg)
            return {
                "status": "error",
                "error": msg,
                "data": {"param_of": param_of},
            }
        return answer

    except Exception as e:
        print(param_path, e)
        traceback.print_exc()
        return {
            "status": "error",
            "error": "search_last_value_still_valid_on" + str(e),
            "data": {"param_of": param_of},
        }


class LegIA:
    def __init__(self, model="meta-llama/Llama-3.3-70B-Instruct-Turbo-Free"):
        self.model = model

    system_prompt_value_extraction = prompts.system_prompt_value_extraction
    system_prompt_date_extraction = prompts.system_prompt_date_extraction

    def llm_find_date_application(self, text: str):
        if len(text) < 10:
            return {
                "question": text,
                "answer": None,
                "date": None,
                "error": "Texte trop court",
            }

        user_prompt = f'Quelle est la date d\'application de la loi suivante ?\n\n"""\n{text}\n"""\n'

        answer = get_completion(
            user_prompt,
            model=self.model,
            system_prompt=self.system_prompt_date_extraction,
        )
        try:
            return {
                "question": user_prompt,
                "answer": answer,
                "date": extract_dates(answer)[-1],
                "error": None,
            }
        except Exception as e:
            return {
                "question": user_prompt,
                "answer": answer,
                "date": None,
                "error": str(e),
            }

    @staticmethod
    def find_value_in_answer(answer: str, brackets=False, debug: bool = False):
        if answer is None:
            return None
        final_answer = None

        try:
            if isinstance(answer, dict):
                final_answer = answer["valeur"]
            else:
                # Handle multiple ```json code blocks and keep the last valid JSON block.
                if "```json" in answer:
                    matches = re.findall(r"```json(.*?)```", answer, re.DOTALL)
                    valid = None
                    # Iterate in reverse order to get the last valid JSON block.
                    for candidate in reversed(matches):
                        try:
                            candidate_json = json.loads(candidate.strip())
                            valid = candidate_json
                            break  # use the first valid JSON from reverse order
                        except Exception:
                            continue
                    if valid is not None:
                        if brackets:
                            if valid.get("brackets"):
                                final_answer = valid
                        else:
                            final_answer = valid.get("valeur")
                # Fallback: extract the first found JSON-like object if no ```json was valid.
                if final_answer is None and "{" in answer:
                    start = answer.find("{")
                    answer_segment = answer[start : answer.find("}") + 1]
                    try:
                        parsed = json.loads(answer_segment)
                        if brackets:
                            if parsed.get("brackets"):
                                final_answer = parsed
                        else:
                            final_answer = parsed.get("valeur")
                    except Exception:
                        pass
        except Exception as e:
            if debug:
                print("llm_for_value - JSON error : ", answer, e)

        if final_answer is None and not isinstance(answer, dict):
            start = answer.find("a nouvelle valeur est :")
            if start != -1:
                start += len("a nouvelle valeur est :")
                final_answer = answer[start:]
        return final_answer

    @staticmethod
    def final_answer(
        system_prompt: str,
        user_prompt: str,
        answer: str,
        final_answer,
        brackets=False,
        debug: bool = False,
    ):
        """
        Format the extracted answer to return a structured response.
        """
        if final_answer is None:
            if debug:
                print("Pas de valeur trouvée !", answer)
            return {
                "question": system_prompt + "\n------------------\n" + user_prompt,
                "answer": answer,
                "value": None,
                "error": "Pas de valeur trouvée !",
            }
        try:
            if brackets:
                return {
                    "question": system_prompt + user_prompt,
                    "answer": answer,
                    "value": final_answer,
                    "error": None,
                }
            else:
                return {
                    "question": system_prompt + user_prompt,
                    "answer": answer,
                    "value": extract_floats(final_answer, debug)[-1],
                    "error": None,
                }
        except Exception as e:
            if debug:
                print(answer, e)
            return {
                "question": system_prompt + "\n------------------\n" + user_prompt,
                "answer": answer,
                "value": None,
                "error": str(e),
            }

    def llm_for_value(
        self, system_prompt: str, user_prompt: str, brackets=False, debug: bool = False
    ):
        # GPT
        # answer = get_completion(user_prompt)
        answer = get_completion(
            user_prompt, model=self.model, system_prompt=system_prompt
        )
        final_answer = self.find_value_in_answer(answer, brackets=brackets, debug=debug)
        return self.final_answer(
            system_prompt,
            user_prompt,
            answer,
            final_answer,
            brackets=brackets,
            debug=debug,
        )

    def llm_find_new_value_from_old_value(
        self,
        description: str,
        previous_legal_text: str,
        new_legal_text: str,
        previous_value: str,
        debug: bool = False,
    ):
        user_prompt = (
            f"Dans le texte suivant, quelle est la valeur de '{description}' sachant que la valeur précédente était '{previous_value}' ?\n"
            + f'\n"""\n{new_legal_text}\n"""\n'
            + "Le contenu du texte précédent, qui n'est plus en vigeur aujourd'hui, était :\n"
            + f'\n"""\n{previous_legal_text}\n"""\n'
        )
        return self.llm_for_value(
            self.system_prompt_value_extraction, user_prompt, debug=debug
        )

    def llm_find_value(
        self,
        description: str,
        new_legal_text: str,
        existing_value: str,
        debug: bool = False,
    ):
        user_prompt = (
            f"Dans le texte suivant, quelle est la valeur de '{description}' sachant que la valeur à une autre date de validité était '{existing_value}' ?\n"
            + f'\n"""\n{new_legal_text}\n"""\n'
        )
        return self.llm_for_value(
            self.system_prompt_value_extraction, user_prompt, debug=debug
        )

    def llm_find_brakets(
        self,
        description: str,
        new_legal_text: str,
        current_brackets: dict,
        debug: bool = False,
    ):
        user_prompt = (
            f"Dans le texte suivant, il faut trouver le nouveau barème pour '{description}' sachant que le barème dont nous avions connaissance était "
            + f"""
            ```json
            {{
                "brackets": {current_brackets}
            }}
            ```
            """
            + f'\n"""\n{new_legal_text}\n"""\n'
        )
        return self.llm_for_value(
            self.system_prompt_value_extraction, user_prompt, brackets=True, debug=debug
        )

    def get_date_article(self, article: str):
        note = article.get("note")
        reponse = self.llm_find_date_application(note)
        date_debut = reponse.get("date")
        if date_debut is None:
            print("WARNING", reponse)
            if article.get("date_debut"):
                return {
                    "date": article.get("date_debut"),
                    "id": article.get("id"),
                    "titre_court": article.get("titre_court"),
                    "note": note,
                }
            if article.get("decret_modificatif") is not None:
                return {
                    "date": article.get("decret_modificatif").get("date"),
                    "id": article.get("decret_modificatif").get("id"),
                    "titre_court": article.get("titre_court"),
                    "note": note,
                }
            return False
        return {
            "date": date_debut,
            "id": article.get("id"),
            "titre_court": article.get("titre_court"),
            "note": note,
        }

    def reformulate_parameter_description(self, param_description: str = None) -> str:
        answer = answer = get_completion(
            prompts.reformulator_prompt(param_description), model=self.model
        )
        reformulated = (
            answer.content.replace("<reformulation>", "")
            .replace("</reformulation>", "")
            .strip()
        )
        reformulated = reformulated.replace('"', "")
        reformulated = reformulated.replace("[", "")
        reformulated = reformulated.replace("]", "")
        return reformulated


if __name__ == "__main__":
    legia = LegIA()
    # Test 1
    result = legia.llm_find_date_application(
        "La loi du 12/03/2022 est applicable à partir du 01/01/2023."
    )
    tc.assertEqual(result["date"], "2023-01-01")
    # Test 2
    result = legia.llm_find_new_value(
        description="le taux de TVA",
        previous_legal_text="Le taux de TVA est de 0.2.",
        new_legal_text="Le taux de TVA est de 0.25.",
        previous_value="0.2",
        debug=True,
    )
    tc.assertEqual(result["value"], 0.25)
    print("Tests pass.")
