import os
import re
import ruamel.yaml
import datetime
from update_openfisca_ai.utilitaires import formatNumber, clean_texte
import urllib.parse
import math
import pandas as pd
import requests
import bisect
import json
from update_openfisca_ai.legidb import extract_id_from_url
from typing import Union

yaml = ruamel.yaml.YAML()
yaml.width = 800
yaml.preserve_quotes = True
md = ""

MINIMAL_DESCRIPTION_LENGTH = 25

openfisca_parameters = None


def remove_unused_parameters_from_df(
    df: pd.DataFrame, unused_list: list = None
) -> pd.DataFrame:
    if unused_list is None:
        with open("../datasets/of-france_unused_parameters.json", "r") as f:
            unused_list = json.load(f)
    df = df[~df["name"].isin(unused_list)]
    return df


def get_openfisca_parameters():
    """Récupère la liste des variables OpenFisca."""
    # La variable est globale pour éviter de la recharger à chaque fois
    global openfisca_parameters
    if openfisca_parameters is None:
        r = requests.get(
            url="https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-openfisca-json/-/raw/master/raw_processed_parameters.json?ref_type=heads",
            timeout=2,
        )
        assert r.status_code == 200
        openfisca_parameters = r.json()
    return openfisca_parameters


def _handle_brackets(brackets: dict):
    """
    Handle brackets in OpenFisca parameter.
    Retourne le barême en vigeur.
    Par exemple, pour ce barême :
    brackets = [
                        {
                            "threshold": {"1945-01-01": {"value": 0}},
                            "rate": {"1945-01-01": {"value": 0}},
                        },
                        {
                            "threshold": {
                                "1982-01-01": {"value": 195000},
                                "1983-01-01": {"value": 212750},
                                "1984-01-01": {"value": 228920},
                                "1985-01-01": {"value": 241740},
                                "1986-01-01": {"value": None},
                            },
                            "rate": {
                                "1982-01-01": {"value": 0.65},
                                "1986-01-01": {"value": None},
                            },
                        },
                        {
                            "threshold": {
                                "1982-01-01": {"value": 11000},
                            },
                            "rate": {
                                "1982-01-01": {"value": 0.11},
                            },
                        },
                        {
                            "threshold": {
                                "1982-01-01": {"value": 33000},
                            },
                            "rate": {
                                "1982-01-01": {"value": 0.33},
                            },
                        },
                    ],
    On voudrait récupérer le dernier barême en vigueur, soit :
    {
        "1982-01-01": [
            {"threshold": 11000, "rate": 0.11},
            {"threshold": 33000, "rate": 0.33}
        ]
    }
    """
    today = datetime.date.today().strftime("%Y-%m-%d")
    last_bracket_values = []
    last_value_date = "0001-01-01"
    for bracket in brackets:
        # Pour chaque bracket, on récupère la dernière valeur
        # Et le dernier threshold
        current_bracket = {}
        last_value_date_rate = "0001-01-01"
        for date, value in bracket.get("rate", {}).items():
            if last_value_date_rate < date < today:
                last_value_date_rate = date
                current_bracket["rate"] = value.get("value")
        last_value_date_thres = "0001-01-01"
        for date, value in bracket.get("threshold", {}).items():
            if last_value_date_thres < date < today:
                last_value_date_thres = date
                current_bracket["threshold"] = value.get("value")
                if last_value_date_thres != last_value_date_rate:
                    last_value_date_rate = max(
                        last_value_date_rate, last_value_date_thres
                    )
        current_bracket["date"] = last_value_date_rate
        last_value_date = max(last_value_date_rate, last_value_date)
        if (
            current_bracket
            and current_bracket.get("rate") is not None
            and current_bracket.get("threshold") is not None
        ):
            last_bracket_values.append(current_bracket)
    return last_bracket_values, last_value_date
    # if last_value_date == "0001-01-01":
    #     print("name", name)
    #     result["neutralises"].append(
    #         {
    #             "name": name,
    #             "description": description,
    #             "github_url": f'https://github.com/openfisca/openfisca-france/blob/master/openfisca_france/parameters/{name.replace(".", "/")}.yaml',
    #         }
    #     )
    # else:
    #     result["actifs_avec_ref"].append(
    #         {
    #             "name": name,
    #             "description": description,
    #             "github_url": f'https://github.com/openfisca/openfisca-france/blob/master/openfisca_france/parameters/{name.replace(".", "/")}.yaml',
    #             "bracket": last_bracket_values,
    #         }
    #     )


def is_brackets_equal(previous, new):
    """
    Check if two brackets are equal.
    """
    if previous is None and new is None:
        return True
    if previous is None or new is None:
        return False
    if len(previous) != len(new):
        return False
    # Is all new existing in previous ?
    for b in new:
        threshold = b.get("threshold")
        rate = b.get("rate")
        for prev in previous:
            if threshold == prev.get("threshold"):
                if rate == prev.get("rate"):
                    break
        else:
            return False
    # Is all previous existing in new ?
    for b in previous:
        threshold = b.get("threshold")
        rate = b.get("rate")
        for n in new:
            if threshold == n.get("threshold"):
                if rate == n.get("rate"):
                    break
        else:
            return False
    # for i, bracket in enumerate(previous):
    #     if bracket != new[i]:
    #         return False
    return True


def update_param_value(
    param_of,
    new_article,
    llm_answer,
    legia,
    legidb,
    root_path="../../openfisca-france/",
    update_historique=False,
):
    param_path = param_of["file_path"]
    """
    Met à jour le paramètre avec :
        * la nouvelle valeur
        * la nouvelle date d'application
        * la nouvelle référence et son décret
    """
    if new_article.get("decret_modificatif") and new_article.get(
        "decret_modificatif"
    ).get("nom"):
        decret = {
            "nom": new_article["decret_modificatif"]["nom"],
            "id": new_article["decret_modificatif"]["id"],
        }
    else:
        decret = None
    update_openfisca_parameter(
        param_path,
        root_path=root_path,
        last_value_still_valid_on_new=datetime.date.today().strftime("%Y-%m-%d"),
        last_value_still_valid_on_old=param_of["last_value_still_valid_on"],
        id_new_ref=new_article["id_version_en_vigueur"],
        date_application=new_article["date_debut"],
        new_value=llm_answer["value"],
        decret=decret,
        titre_court=new_article["titre_court"],
    )
    if update_historique:
        # Nous allons aussi mettre à jour l'historique des valeurs
        param_dict = get_param(param_path, root_path=root_path)
        historique = recupere_historique_param_dict(
            new_article["id"],
            param_dict,
            legia,
            legidb,
            param_path,
            root_path,
            debug=True,
        )
        save_historique(param_path, historique, root_path=root_path, overwrite=True)


def _process_a_param_from_json(param: str, content: dict, path: str, result: dict):
    """
    Process a parameter from the JSON file.
    Function to be used in a recursive way to process all the parameters.
    return brackets, values, continue_loop, stop_loop
    """
    brackets, values = None, None
    stop_loop = True
    continue_loop = True
    if isinstance(content, str):
        return brackets, values, continue_loop, False
    if param in ["__pycache__"]:
        return brackets, values, continue_loop, False
    if param in ["metadata", "values", "brackets"]:
        return brackets, values, continue_loop, False

    path = path + "." + param if path else param
    # print(path)
    # if nb_param> 200:
    #     break
    if isinstance(content, list):
        print(path, "List : ", content)
        return None, None, False, stop_loop
    description = content.get("description")
    documentation = content.get("documentation")
    name = content.get("name")
    values = content.get("values")
    brackets = content.get("brackets")
    value = None
    last_value_date = None
    last_value_still_valid_on = None
    if values and brackets:
        raise ValueError("values and brackets !")
    if values:
        # Recupère la dernière valeur
        for k, v in values.items():
            value = v.get("value")
            last_value_date = k
        if last_value_date and value is None:
            # Le parametre est neutralisé : inutile de le garder
            # print(name)
            result["neutralises"].append(
                {
                    "name": name,
                    "description": description,
                    "github_url": f'https://github.com/openfisca/openfisca-france/blob/master/openfisca_france/parameters/{name.replace(".", "/")}.yaml',
                }
            )
            return brackets, values, False, False
    if brackets:
        # On remplace les brackets par le dernier barême en vigueur
        brackets, last_value_date = _handle_brackets(brackets)
    no_last_value_still_valid_on = True
    old_last_value_check = None
    file_path = content.get("file_path")
    if content.get("metadata"):
        reference = content.get("metadata").get("reference")
        last_value_still_valid_on = content.get("metadata").get(
            "last_value_still_valid_on"
        )
        if last_value_still_valid_on:
            no_last_value_still_valid_on = False
            date_object = datetime.datetime.strptime(
                last_value_still_valid_on, "%Y-%m-%d"
            )
            current_date = datetime.datetime.now()

            # Calculate date one year from now
            one_year_from_last_check = date_object + datetime.timedelta(days=365)

            # Check if date_object is more than one year from now
            if current_date > one_year_from_last_check:
                old_last_value_check = True
                result["nb_last_value_expired"] += 1
            else:
                old_last_value_check = False
    else:
        reference = None
    if reference and (brackets or values):
        # Les index peuvent avoir des références alors on vérifie que l'on a brackets ou values.
        result["nb_param_avec_ref"] += 1
        if brackets or values:
            result["param_actif"] += 1
        if reference.get(last_value_date) is None:
            param = {
                "name": name,
                "file_path": file_path,
                "description": description,
                "documentation": documentation,
                "last_value_date": last_value_date,
                "last_value_still_valid_on": last_value_still_valid_on,
                "no_last_value_still_valid_on": no_last_value_still_valid_on,
                "old_last_value_check": old_last_value_check,
                "github_url": f"https://github.com/openfisca/openfisca-france/blob/master/{file_path}",
                "value": value,
            }
            if brackets:
                param["is_brackets"] = True
                param["current_bracket"] = brackets
            else:
                param["is_brackets"] = False
            result["last_ref_manquante"].append(param)
            #     {
            #         "name": name,
            #         "description": description,
            #         "github_url": f'https://github.com/openfisca/openfisca-france/blob/master/openfisca_france/parameters/{name.replace(".", "/")}.yaml',
            #         "last_value_date": last_value_date,
            #     }

            return brackets, values, False, False
        urls = []
        ref = reference.get(last_value_date)
        for r in ref:
            url = r.get("href")
            if url:
                urls.append(url)
                # if ("LEGI" in url or "JORF" in url or "eli/decret" in url):

        if len(urls) > 0:
            param = {
                "name": name,
                "file_path": file_path,
                "description": description,
                "documentation": documentation,
                "last_value_date": last_value_date,
                "last_value_still_valid_on": last_value_still_valid_on,
                "no_last_value_still_valid_on": no_last_value_still_valid_on,
                "old_last_value_check": old_last_value_check,
                "github_url": f"https://github.com/openfisca/openfisca-france/blob/master/{file_path}",
                "id": extract_id_from_url(urls),
                "value": value,
            }
            for i, url in enumerate(urls):
                param[f"url_ref_{i}"] = url
            if brackets:
                param["is_brackets"] = True
                param["current_bracket"] = brackets
            else:
                param["is_brackets"] = False
            result["actifs_avec_ref"].append(param)
        # break
    elif (brackets or values) and not reference:
        param = {
            "name": name,
            "file_path": file_path,
            "description": description,
            "documentation": documentation,
            "last_value_date": last_value_date,
            "last_value_still_valid_on": last_value_still_valid_on,
            "no_last_value_still_valid_on": no_last_value_still_valid_on,
            "old_last_value_check": old_last_value_check,
            "github_url": f'https://github.com/openfisca/openfisca-france/blob/master/openfisca_france/parameters/{name.replace(".", "/")}.yaml',
            "value": value,
            "is_brackets": False,
        }
        if brackets:
            param["is_brackets"] = True
            param["current_bracket"] = brackets
        result["no_ref"].append(param)

        return brackets, values, False, False
    return brackets, values, False, False


def get_params_from_openfisca_france_json(openfisca_parameters=None):
    """
    Cette fonction permet de récupérer les paramètres d'OpenFisca France à partir du fichier JSON généré par OpenFisca-France-JSON.
    Elle va les classer dans des dictionnaires pour faciliter leur manipulation.
    """
    result = {
        "nb_param": 0,
        "nb_param_avec_ref": 0,
        "nb_last_value_expired": 0,
        "param_actif": 0,
        "neutralises": [],
        "actifs_avec_ref": [],
        "last_ref_manquante": [],
        "no_ref": [],
    }

    openfisca_parameters = (
        openfisca_parameters if openfisca_parameters else get_openfisca_parameters()
    )

    def recursive_get_param(param_dict, path=None):
        nonlocal result
        if isinstance(param_dict, str):
            raise ValueError(f"param_dict is a string {param_dict=} {path=}")
        for param, content in param_dict.items():
            brackets, values, continue_loop, stop_loop = _process_a_param_from_json(
                param, content, path, result
            )
            if continue_loop:
                continue
            if stop_loop:
                break
            if brackets is None and values is None:
                # print("pas de ref")
                recursive_get_param(content, path)
            else:
                result["nb_param"] += 1

    recursive_get_param(openfisca_parameters, path=None)
    return result


def get_full_description(param_path, root_path):
    """
    Get the full description of an OpenFisca parameter by exploring index.yaml of all the upper branch.
    """
    folders = param_path.split("/")[2:]
    folders
    path_hist = "openfisca_france/parameters/"
    descriptions = []
    for folder in folders:
        path = os.path.join(path_hist, folder)
        if "yaml" in folder:
            index_param = get_param(chemin=path, root_path=root_path)
        else:
            index_param = get_param(chemin=path + "/index.yaml", root_path=root_path)

        descriptions.append(index_param["description"])
        if index_param.get("documentation"):
            descriptions.append(index_param["documentation"])
        path_hist = os.path.join(path_hist, folder)
    return descriptions


def get_values_param_dict(param_dict):
    """
    Get the values of an OpenFisca parameter.
    And keep the last value as a sample_value.
    """
    param_values = {}
    if param_dict is None or param_dict.get("values") is None:
        print("WARNING: No values for ", param_dict)
        return param_values
    for date, value in param_dict["values"].items():
        date = str(date)
        param_values[date] = value
        sample_value = value
    return {
        "values": param_values,
        "sample_value": sample_value,
    }


def deduplicate_historique(historique):
    """
    De-duplicate if parameter value don't change : For all parameter keep only the first value change, the following values must be deleted.
    If the input is
    {
        "2022-01-01": {"value": 100, "fin": "2022-01-31"},
        "2022-02-01": {"value": 100, "fin": "2022-02-28"},
        "2022-03-01": {"value": 100, "fin": "2022-03-31"},
    }
    The output will be
    {
        "2022-01-01": {"value": 100, "fin": "2022-03-31"},
    }
    """
    keys = list(historique.keys())
    keys.sort()
    new_historique = {}
    last_value = None
    last_key = None
    for date in keys:
        param_at_date = historique[date]
        if last_value == param_at_date["value"]:
            # Les paramètres existants n'ont pas de date de fin
            if last_key in new_historique:
                new_historique[last_key]["fin"] = param_at_date.get("fin")
            continue
        new_historique[date] = param_at_date
        last_value = param_at_date["value"]
        last_key = date
    return new_historique


def split_openfisca_parameter(param, output_path):
    """
    Take one OpenFisca parameter and split it into multiple files : one file per parameter.
    """
    for name, content in param.items():
        if name in ("description", "documentation"):
            continue
        print(name)
        if not os.path.exists(f"{output_path}"):
            os.makedirs(f"{output_path}")
        with open(f"{output_path}/{name}.yaml", "w") as f:
            yaml.dump(content, f)


def convert_str_to_date(date_str: str) -> datetime.date:
    date_in_dt = datetime.datetime.strptime(date_str, "%Y-%m-%d")
    return datetime.date(date_in_dt.year, date_in_dt.month, date_in_dt.day)


def create_url_legifrance(
    url_legi: str = None,
    id_legifrance: str = None,
    date: str = None,
    value_in_ref: str = None,
) -> str:
    """
    Create a URL to the Legifrance website.
    """
    if value_in_ref:
        encoded_value = urllib.parse.quote_plus(str(value_in_ref)).replace("+", "%20")
    else:
        encoded_value = None
    if id_legifrance and encoded_value and date:
        url = f"https://www.legifrance.gouv.fr/codes/article_lc/{id_legifrance}/{date}/#:~:text={encoded_value}"
    elif id_legifrance and date:
        url = f"https://www.legifrance.gouv.fr/codes/article_lc/{id_legifrance}/{date}/"
    elif url_legi and date and encoded_value:
        url = f"{url_legi}/{date}/#:~:text={encoded_value}"
    elif url_legi and date:
        url = f"{url_legi}/{date}/"
    elif url_legi and encoded_value:
        url = f"{url_legi}/#:~:text={encoded_value}"
    return url


def check_same_value(value: float, value_in_ref: str) -> bool:
    """
    Check if the value is the same as the value in the reference.
    """
    if not isinstance(value_in_ref, float):
        try:
            value_in_ref = float(value_in_ref.replace("demi", "0.5"))
        except ValueError:
            try:
                value_in_ref = float(value_in_ref.replace(",", ".").replace(" ", ""))
            except ValueError:
                print(f"ValueError: {value_in_ref}")
                return False
    try:
        if value == value_in_ref:
            return True
        elif math.isclose(value * 100, value_in_ref):
            return True
    except ValueError:
        print(f"ValueError: {value} != {value_in_ref}")
        return False
    return False


def df_to_markdown(
    df, mode: str = "", include_list: list = None, exclude_list: list = None
):
    """
    Génère un tableau Markdown à partir d'un DataFrame pour l'inclure dans la description d'une PR
    """
    global md
    md = ""
    df_filtered = df
    if exclude_list:
        df_filtered = df_filtered[~df_filtered["name"].isin(exclude_list)]
    if include_list:
        df_filtered = df_filtered[df_filtered["name"].isin(include_list)]

    def to_md_last_value(row):
        """
        Génére un tableau Markdown pour les cas où il faut juste mettre à jour le champ last_value_still_valid_on.
        """
        global md
        url = row["url_ref_with_value"]
        if pd.isna(url) or url == "" and row["url_ref_0"] != "":
            url = "Valeur non trouvée dans " + create_url_legifrance(
                url_legi=row["url_ref_0"], date=row["last_value_still_valid_on"]
            )
        else:
            url = "Valeur trouvée dans " + create_url_legifrance(
                url_legi=row["url_ref_with_value"],
                date=row["last_value_still_valid_on"],
            )
        md += f"| {row['description']} [{row['name']}]({row['github_url']})| {row['last_value_still_valid_on']} | {row['value']} | {row['value_in_ref']} | {url} |  \r\n"
        return row

    def to_md_new_value(row):
        """
        Génère un tableau Markdown pour les cas où il faut mettre à jour la valeur, en plus de last_value_still_valid_on.
        """
        global md
        if row["value_in_nouveau_ref_found"] == "":
            row["to_change"] = 0
            return row
        url = create_url_legifrance(
            id_legifrance=row["id_version_en_vigueur"],
            date=row["date_application"],
            value_in_ref=row["value_in_nouveau_ref_found"],
        )
        url_field = f'[{row["id_version_en_vigueur"]}]({url})'
        new_value = float(
            str(row["value_in_nouveau_ref_float"]).replace(",", ".").replace(" ", "")
        )
        value_in_nouveau_ref_texte = clean_texte(row["value_in_nouveau_ref_texte"])

        md += f"| {row['description']} [{row['name']}]({row['github_url']}) | {row['value']} | {value_in_nouveau_ref_texte} | {new_value} | {url_field} |\r\n"
        row["to_change"] = 1
        return row

    if mode == "new_value":
        md = "| Paramètre | Ancienne valeur | Extrait |  Nouvelle valeur trouvée | Valeur trouvée dans | \r\n"
        md += "| --- | --- | --- | --- | --- |  \r\n"
        _ = df_filtered.apply(to_md_new_value, axis=1)
    elif mode == "last_value":
        md = "| Paramètre | last_value_still_valid_on | valeur | valeur trouvée dans le texte | Référence |  \r\n"
        md += "| --- | --- | --- | --- | --- |  \r\n"
        _ = df_filtered.apply(to_md_last_value, axis=1)
    else:
        print(f"df_to_markdown - Unknown mode {mode}")
    return md


def sort_CommentedMap(to_sort: ruamel.yaml.CommentedMap) -> ruamel.yaml.CommentedMap:
    date_asc = sorted(list(to_sort))
    sorted_value = ruamel.yaml.CommentedMap()
    for dk in date_asc:
        sorted_value[dk] = to_sort[dk]
    return sorted_value


def get_param(
    chemin: str,
    root_path: str = "../../openfisca-france/",
):
    filepath = os.path.join(root_path, chemin)
    if not os.path.exists(filepath):
        print(f"File not found {filepath}")
        return None
    with open(filepath) as f:
        of_param = yaml.load(f)
    return of_param


def get_values(
    chemin: str,
    root_path: str = "../../openfisca-france/",
):
    filepath = os.path.join(root_path, chemin)
    if not os.path.exists(filepath):
        print(f"File not found {filepath}")
        return None
    with open(filepath) as f:
        of_param = yaml.load(f)
    param_values = {}
    for date, value in of_param["values"].items():
        date = str(date)
        param_values[date] = value
        sample_value = value
    return {
        "values": param_values,
        "sample_value": sample_value,
    }


def update_openfisca_parameter(
    chemin: str,
    last_value_still_valid_on_new: str,
    last_value_still_valid_on_old: str = None,
    id_new_ref: str = None,
    new_value: Union[float, dict] = None,
    date_application: str = None,
    titre_court: str = None,
    new_references: dict = None,
    description: str = None,
    short_label: str = None,
    decret: dict = None,
    root_path: str = "../../openfisca-france/",
    overwrite: bool = False,
):
    """
    Update an OpenFisca parameter file.
    Args:
        chemin (str): Relative path to the OpenFisca parameter file.
        last_value_still_valid_on_old (str): Old date of the last value still valid.
        last_value_still_valid_on_new (str): New date of the last value still valid.
        id_new_ref (str): ID Legifrance of the new reference.
        new_value (float or dict): New value.
        date_application (str): Date of application of the new value.
        titre_court (str): Short title of the new reference.
        new_references (dict): Nouvelle référence, sous la forme :
          [ {
                "title": "titre1",
                "href": "https://url1",
            }.
            {
                "title": "titre2",
                "href": "https://url2",
            }
          ]
        description (str): Description of the parameter.
        short_label (str): Short label of the parameter.
        decret (dict): Decret pour la référence à la date de la valeur. Sous la forme :
          { "nom": "nom du décret", "id": "id du décret" }
        root_path (str): Root path of the OpenFisca project.
        overwrite (bool): Overwrite the existing value.

    If new_value is a dict, it must be in the form :
        {"rate": 0, "threshold": 0},
        {"rate": 0.11, "threshold": 11983},
        {"rate": 0.33, "threshold": 33000},
    That will be converted to YAML :
    brackets:
    - threshold:
        1945-01-01:
        value: 0
    rate:
        1945-01-01:
        value: 0
    - threshold:
        2025-01-01:
        value: 11000
    rate:
        2025-01-01:
        value: 0.11
    - threshold:
        2025-01-01:
        value: 33000
    rate:
        2025-01-01:
        value: 0.33

    """
    filepath = os.path.join(root_path, chemin)
    is_brackets = False
    if not os.path.exists(filepath):
        print(f"File not found {filepath}")
        return None
    if new_value:
        if isinstance(new_value, dict) and "brackets" in new_value:
            new_value = new_value["brackets"]
            is_brackets = True
        else:
            try:
                new_value = float(str(new_value).replace(",", ".").replace(" ", ""))
                new_value = formatNumber(new_value)
            except Exception:
                print(f"new_value is not a float {new_value}")
                return None
    if date_application == "":
        print(f"date_application is empty for {id_new_ref}")
        return None

    with open(filepath) as f:
        of_param = yaml.load(f)

    if description:
        # S'il y a déjà une description et qu'elle est courte, on la déplace dans short_label
        # et on la remplace par celle donnée en paramètre
        if (
            of_param.get("description")
            and overwrite
            and len(of_param["description"]) < MINIMAL_DESCRIPTION_LENGTH
        ):
            if of_param.get("short_label") is None:
                of_param["metadata"]["short_label"] = of_param["description"]
            of_param["description"] = description
        else:
            of_param["description"] = description

    if short_label:
        if (
            of_param.get("metadata")
            and of_param["metadata"].get("short_label")
            and not overwrite
        ):
            pass
        else:
            if of_param.get("metadata") is None:
                of_param["metadata"] = {}
            of_param["metadata"]["short_label"] = short_label

    if last_value_still_valid_on_old is None:
        if of_param.get("metadata"):
            last_value_still_valid_on_old = of_param["metadata"].get(
                "last_value_still_valid_on"
            )
    if date_application:
        if isinstance(date_application, datetime.date):
            date_application_in_datetime = date_application
        else:
            date_application_in_datetime = convert_str_to_date(date_application)
    # Mise à jour de la référence legifrance
    if new_references and date_application:
        if len(new_references) > 1:
            new_ref = new_references
        else:
            new_ref = new_references[0]
        if of_param["metadata"].get("reference") is None:
            of_param["metadata"]["reference"] = {}

        of_param["metadata"]["reference"][date_application] = new_ref
    if id_new_ref and date_application:
        if of_param["metadata"].get("reference") is None:
            of_param["metadata"]["reference"] = {}
        if of_param["metadata"]["reference"].get(date_application_in_datetime):
            if overwrite is False:
                print("Une référence existe déjà !")
        else:
            new_ref = {
                "title": f"{titre_court}",
                "href": f"https://www.legifrance.gouv.fr/codes/article_lc/{id_new_ref}/{date_application}/",
            }
            if decret:
                new_ref = [
                    new_ref,
                    {
                        "title": decret.get("nom"),
                        "href": f"https://www.legifrance.gouv.fr/jorf/id/{decret.get('id')}",
                    },
                ]
            of_param["metadata"]["reference"][date_application] = new_ref
    # Mise à jour de la valeur
    if new_value and date_application:
        if is_brackets:
            if of_param.get("brackets") is None:
                raise ValueError(
                    "brackets not found in the parameter, but you want to update it"
                )
            updated_brackets = update_brackets(
                of_param.get("brackets"), new_value, date_application
            )
            of_param["brackets"] = updated_brackets
        else:
            # Check if value already exist
            last_value = (
                get_values_param_dict(of_param).get("sample_value").get("value")
            )
            if last_value == new_value:
                print(f"{last_value=} == {new_value=}, so we do not insert it !")
            else:
                if of_param["values"].get(date_application_in_datetime):
                    if overwrite is False:
                        print(
                            "update_openfisca_parameter - Une valeur existe déjà !",
                            of_param["values"][date_application_in_datetime]["value"],
                        )
                    else:
                        of_param["values"][date_application_in_datetime][
                            "value"
                        ] = new_value
                else:
                    of_param["values"][date_application] = {"value": new_value}
    with open(filepath, "w") as f:
        yaml.dump(of_param, f)

    # Lecture du fichier en tant que texte brut pour supprimer les ' dans les clefs de date.
    # Et mettre à jour last_value_still_valid_on
    with open(filepath) as f:
        content = f.read()
    # Suppression des ' introduite par ruamel.yaml
    content = content.replace(f"'{date_application}'", f"{date_application}")

    # Mise à jour de last_value_still_valid_on
    if content.find("last_value_still_valid_on") > 0:
        content = content.replace(
            f'last_value_still_valid_on: "{last_value_still_valid_on_old}"',
            f'last_value_still_valid_on: "{last_value_still_valid_on_new}"',
        )
    else:
        content = content.replace(
            "metadata:",
            f'metadata:\n  last_value_still_valid_on: "{last_value_still_valid_on_new}"',
        )
    # Ecriture du fichier
    with open(filepath, "w") as f:
        f.write(content)

    # On remet dans l'ordre les dates
    # Ca ne fonctionne pas quand il y a un mix entre les dates et des date en string
    with open(filepath) as f:
        of_param = yaml.load(f)
    if of_param.get("metadata") and of_param["metadata"].get("reference"):
        of_param["metadata"]["reference"] = sort_CommentedMap(
            of_param["metadata"]["reference"]
        )
    if not is_brackets:
        of_param["values"] = sort_CommentedMap(of_param["values"])
    with open(filepath, "w") as f:
        yaml.dump(of_param, f)

    # On remet les null à la place des valeurs vides, il faut utiliser une regexp avec une fin de chaines
    with open(filepath) as f:
        content = f.read()
    content = re.sub(r"value:\s*$", "value: null", content, flags=re.MULTILINE)
    with open(filepath, "w") as f:
        f.write(content)
    return filepath


def get_better_description(
    value: dict,
    of_param_description: str,
    param_path: str,
    root_path: str,
    legia,
    debug=False,
):
    # On regarde s'il pourrait y avoir une meilleure description
    if len(of_param_description) < MINIMAL_DESCRIPTION_LENGTH:
        value["short_label"] = of_param_description
        # On reconstitue la description la plus complète
        descriptions = get_full_description(param_path, root_path=root_path)
        # On demande à un LLM de la reformuler
        new_param_description = legia.reformulate_parameter_description(descriptions)
        if (
            new_param_description is None
            or len(new_param_description) < MINIMAL_DESCRIPTION_LENGTH
        ):
            print(f"ERROR for {value["short_label"]} ", of_param_description)
            return of_param_description
        if debug:
            print(
                f"get_better_description - Updating description to {of_param_description} with {new_param_description}"
            )
        return new_param_description
    else:
        return of_param_description


def recupere_historique_param_dict(
    id, param_dict, legia, legidb, param_path, root_path, debug=False
):
    # Mise à jour de l'historique
    param_values = get_values_param_dict(param_dict)
    of_param_description = param_dict["description"]
    sample_value = param_values["sample_value"]
    param_values = param_values["values"]
    raw_article_json = legidb.get_raw_article_json(id)
    if not raw_article_json:
        print(f"recupere_historique_param_dict - No article found for {id}")
        return {}
    legi_versions = legidb.get_versions(raw_article_json)
    if len(legi_versions) == 0:
        print(f"recupere_historique_param_dict - No version found for {id}")
        return {}
    values_to_add = {}
    for date, version in legi_versions.items():
        values_to_add[date] = version
    for date, value in values_to_add.items():
        if debug:
            print(f"recupere_historique_param_dict - Processing {date}")
        article = legidb.get_article(value["id"])
        if not article:
            print(
                f"recupere_historique_param_dict - Article not found for {value['id']}"
            )
            continue
        value["etat"] = article["etat"]
        value["type"] = article["type"]
        value["nature"] = article["nature"]
        value["decret"] = article["decret_modificatif"]
        value["description"] = get_better_description(
            value, of_param_description, param_path, root_path, legia, debug=False
        )
        llm_answer = legia.llm_find_value(
            description=of_param_description,
            new_legal_text=article["content_html"],
            existing_value=sample_value,
            debug=False,
        )
        value["value"] = llm_answer["value"]
        if value["value"] is None:
            print(f"ERROR for {date}", llm_answer)
            continue
        # print("note=", article.get("note"))
        if len(article.get("note")) < 10:
            value["date_application"] = value["debut"]
            value["titre_court"] = article["titre_court"]
            if debug:
                print(
                    f"recupere_historique_param_dict - Adding value {value['value']} for {date} - len(Note) < 10 : {article.get('note')}"
                )
            continue
        metadata = legia.get_date_article(article)
        if metadata is False:
            print(f"Pas de date d'application trouvée pour {date} dans {id}")
            continue
        value["date_application"] = metadata["date"]
        value["titre_court"] = metadata["titre_court"]
        if debug:
            print(
                f"recupere_historique_param_dict - Adding value {value['value']} for {date} - len(Note) > 10 : {article.get('note')}"
            )
    historique = values_to_add
    for date, v in param_values.items():
        if historique.get(date) is None:
            historique[date] = v

    return historique


def save_historique(
    filepath, historique, root_path="../../openfisca-france/", overwrite: bool = False
):
    historique = deduplicate_historique(historique)
    md = None
    for date, value in historique.items():
        if value["value"] is None or value.get("date_application") is None:
            # print(f"ERROR for {date} : {value=} {value.get('date_application')=}")
            continue
        print(f"Adding version {date} to {filepath}")
        file = update_openfisca_parameter(
            chemin=filepath,
            last_value_still_valid_on_old=None,
            last_value_still_valid_on_new=datetime.date.today().strftime("%Y-%m-%d"),
            id_new_ref=value["id"],
            new_value=value["value"],
            date_application=value["date_application"],
            titre_court=value["titre_court"],
            description=value.get("description"),
            short_label=value.get("short_label"),
            decret=value.get("decret"),
            root_path=root_path,
            overwrite=overwrite,
        )
        if file:
            md = f"    * {file.replace(f'{root_path}openfisca_france', '')}\n"
        else:
            print(f"Erreur lors de la mise à jour de {filepath} pour {date}!")
    return md


def update_brackets(previous_brackets, new_brackets, new_date):
    """
    Update the value of the brackets parameter based on the thresholds.
    For each new slice in new_tranches (a list of dicts of the form {'threshold': int, 'rate': float}),
    add the key new_date with the new value to the 'threshold' dictionary.
    Then, check in the 'rate' dictionary if the rate new_tranches[i]['rate'] already exists.
    If not, add it under new_date. The updates are made for the i-th element of new_tranches in the i-th existing bracket.

    - You must not begin by looking at similar rate : you have first to find the threshold where to put the new threshold value by looking at existing threshold value, took the last value of all thresholds and insert the new threshold value is the threshold having the lowest positive value between existing value and new value.
    - If a same value exist for threshold or rate, you must not add it. Just add element that does not exit yet. For example a new threshold with an existing rate must create only the new threshold entry with a new date, but keep rate as is.
    """

    # Collect existing brackets with their latest threshold values from the original data
    existing_brackets = []
    modified_brackets = previous_brackets.copy()
    for bracket in modified_brackets:
        thresholds = bracket.get("threshold", {})
        if not thresholds:
            continue
        # Sort threshold dates to find the latest
        threshold_dates = sorted(thresholds.keys())
        latest_date = threshold_dates[-1]
        latest_entry = thresholds[latest_date]
        latest_value = latest_entry.get("value")
        # Skip if the latest threshold value is None
        if latest_value is None:
            continue
        existing_brackets.append((latest_value, bracket))

    # Sort existing brackets by their latest threshold value
    sorted_brackets = sorted(existing_brackets, key=lambda x: x[0])
    sorted_values = [br[0] for br in sorted_brackets]

    for new_bracket in new_brackets:
        new_threshold = new_bracket["threshold"]
        # Find the right bracket based on the original data's thresholds
        index = bisect.bisect_right(sorted_values, new_threshold) - 1
        if index >= 0:
            target_bracket = sorted_brackets[index][1]
        else:
            # If new_threshold is smaller than all, use the first bracket
            if not sorted_brackets:
                continue
            target_bracket = sorted_brackets[0][1]

        # Check if the new_threshold already exists in any of the threshold values
        existing_values = [
            v["value"]
            for v in target_bracket["threshold"].values()
            if v["value"] is not None
        ]
        if new_threshold not in existing_values:
            target_bracket["threshold"][new_date] = {"value": new_threshold}
    return modified_brackets
