from update_openfisca_ai.utilitaires import find_value_in_texte


def get_md(param: list[dict]):
    """
    Crée la liste des modificariations apportée à un paramètre.
    """
    p = param["param_of"]
    val_in_txt = find_value_in_texte(
        txt_to_search=param["article"]["content_html"], value_to_find=p["value"]
    )
    try:
        start = max(param["article"]["content_txt"].find(val_in_txt) - 100, 0)
        stop = min(
            param["article"]["content_txt"].find(val_in_txt) + 100,
            len(param["article"]["content_txt"]),
        )
        extrait = "_[...]" + param["article"]["content_txt"][start:stop] + "[...]_"
    except Exception:
        extrait = "Valeur non retrouvée par `find_value_in_texte`"

    return f""" 
 * {p["name"]}
    - Description : {p["description"]}
    - {p["value"]}
    - [Texte de loi]({p["url_ref_0"]}) de type {param["article"].get("nature")}
    - Extrait : {extrait}
    - Reponse du LLM : _{param["llm_answer"]["answer"].replace("<think>\n", "").replace("\n", " ").strip()}_
    """


def create_text_for_update_last_value_still_valid_on(param_still_valid: list[dict]):
    fichiers_impactes = "\n".join(
        [f"  - {p['param_of']['file_path']}" for p in param_still_valid]
    )
    message = f"""
    * Changement mineur.
    * Périodes concernées : 2025.
    * Zones impactées :
    {fichiers_impactes}
    * Détails :
    - Mise à jour des `last_value_still_valid_on`, sur les paramètres dont la nouvelle valeur a pu être trouvée avec un bon niveau de fiabilité.

    - - - -

    Ces changements (effacez les lignes ne correspondant pas à votre cas) :

    - Mise à jour de paramètre.

    - - - -

    Méthodologie :
    1. Récupére les paramètres OpenFisca depuis [leximpact-socio-fiscal-openfisca-json](https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-openfisca-json/-/raw/master/raw_processed_parameters.json?ref_type=heads) et les exportent dans un fichier CSV avec une ligne par paramètres.
    1. Filtre sur les paramètres qui ne sont pas neutralisée, qui ont une référence législative et la date du champ `last_value_still_valid_on` est de plus d'un an.
    1. Regarde dans l'OpenData de Légifrance, via [le GitLab tricoteuses](https://git.en-root.org/tricoteuses/data/dila), si l'article de loi référencé est toujours le dernier en vigueur.
    1. Vérifie la valeur du paramètre en lisant le nouvel article de référence avec un LLM.
    1. Met à jour la `last_value_still_valid_on` à la date du jour.
    1. Crée un tableau récapitulatif pour faciliter la revue.

    Plus d'informations sur le processus de mise à jour des paramètres [sur le Gitlab de LexImpact](https://git.leximpact.dev/leximpact/exploration/update-openfisca-with-ai).

    - - - -

    Quelques conseils à prendre en compte :

    - [X] Jetez un coup d'œil au [guide de contribution](https://github.com/openfisca/openfisca-france/blob/master/CONTRIBUTING.md).
    - [X] Regardez s'il n'y a pas une [proposition introduisant ces mêmes changements](https://github.com/openfisca/openfisca-france/pulls).
    - [X] Documentez votre contribution avec des références législatives.
    - [X] Mettez à jour ou ajoutez des tests correspondant à votre contribution.
    - [ ] Augmentez le [numéro de version](https://speakerdeck.com/mattisg/git-session-2-strategies?slide=81) dans [`pyproject.toml`](https://github.com/openfisca/openfisca-france/blob/master/pyproject.toml).
    - [ ] Mettez à jour le [`CHANGELOG.md`](https://github.com/openfisca/openfisca-france/blob/master/CHANGELOG.md).
    - [X] Assurez-vous de bien décrire votre contribution, comme indiqué ci-dessus
    """

    md = """
    Cette PR propose une mise à jour de paramètres qui sont été identifiés de façon automatique comme n'ayant pas changés depuis la date indiquée dans `last_value_still_valid_on`.

    Aide à la revue :
    """

    for param in param_still_valid:
        md += get_md(param)

    # display(Markdown(md))
    texte = message + md
    return texte
    # texte


def create_text_for_update_values(param_to_update):
    fichiers_impactes = "\n".join(
        [f"  - {p['param_of']['file_path']}" for p in param_to_update]
    )
    message = f"""
    * Changement mineur.
    * Périodes concernées : 2025.
    * Zones impactées :
    {fichiers_impactes}
    * Détails :
    - Mise à jour des `last_value_still_valid_on`, des valeurs et des références sur les paramètres qui n'étaient plus à jour et dont la nouvelle valeur a pu être trouvée avec un bon niveau de fiabilité.

    - - - -

    Ces changements (effacez les lignes ne correspondant pas à votre cas) :

    - Mise à jour de paramètre.

    - - - -

    Méthodologie :
    1. Récupére les paramètres OpenFisca depuis [leximpact-socio-fiscal-openfisca-json](https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-openfisca-json/-/raw/master/raw_processed_parameters.json?ref_type=heads) et les exportent dans un fichier CSV avec une ligne par paramètres.
    1. Filtre sur les paramètres qui ne sont pas neutralisée, qui ont une référence législative et la date du champ `last_value_still_valid_on` est de plus d'un an.
    1. Regarde dans l'OpenData de Légifrance, via [le GitLab tricoteuses](https://git.en-root.org/tricoteuses/data/dila), si l'article de loi référencé est toujours le dernier en vigueur.
    1. Met à jour la valeur du paramètre en lisant le nouvel article de référence avec un LLM.
    1. Met à jour la `last_value_still_valid_on` à la date du jour.
    1. Met à jour la référence législative.
    1. Crée un tableau récapitulatif pour faciliter la revue.

    Plus d'informations sur le processus de mise à jour des paramètres [sur le Gitlab de LexImpact](https://git.leximpact.dev/leximpact/exploration/update-openfisca-with-ai).

    - - - -

    Quelques conseils à prendre en compte :

    - [X] Jetez un coup d'œil au [guide de contribution](https://github.com/openfisca/openfisca-france/blob/master/CONTRIBUTING.md).
    - [X] Regardez s'il n'y a pas une [proposition introduisant ces mêmes changements](https://github.com/openfisca/openfisca-france/pulls).
    - [X] Documentez votre contribution avec des références législatives.
    - [X] Mettez à jour ou ajoutez des tests correspondant à votre contribution.
    - [ ] Augmentez le [numéro de version](https://speakerdeck.com/mattisg/git-session-2-strategies?slide=81) dans [`pyproject.toml`](https://github.com/openfisca/openfisca-france/blob/master/pyproject.toml).
    - [ ] Mettez à jour le [`CHANGELOG.md`](https://github.com/openfisca/openfisca-france/blob/master/CHANGELOG.md).
    - [X] Assurez-vous de bien décrire votre contribution, comme indiqué ci-dessus
    """

    md = """
    Cette PR propose une mise à jour de paramètres qui sont été identifiés de façon automatique comme ayant changés depuis la date indiquée dans `last_value_still_valid_on`.

    Il s'agit des paramètres suivants :
    """
    for param in param_to_update:
        md += get_md(param)

    texte = message + md
    return texte
